package com.arun.aashu.authorbio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by aAsHu on 3/24/2018.
 */


public class MyAdapter extends BaseAdapter {
//
    /*********** Declare Used Variables *********/

    private static LayoutInflater inflater=null;

    private  Activity activity;
    private  ArrayList<Model> data;


    public MyAdapter() {
    }

    /*************  CustomAdapter Constructor *****************/
    public MyAdapter(Activity a, ArrayList<Model> d) {

        /********** Take passed values **********/
        activity = a;
        data=d;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<=0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView authorName,dobText,deathText,biography,books;
        public ImageView authorphoto;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View vi = convertView;
        final ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.jadu, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.authorName = (TextView) vi.findViewById(R.id.authorName);
            holder.authorphoto=(ImageView)vi.findViewById(R.id.authorImage);

//            holder.dobText=vi.findViewById(R.id.dobText);
//            holder.deathText=vi.findViewById(R.id.deathText);
//            holder.biography= vi.findViewById(R.id.biographyText);
//            holder.books= vi.findViewById(R.id.bookstext);


            holder.authorName.setText(data.get(position).getAuthorName());
            holder.authorphoto.setImageResource(data.get(position).getImage());


            if(position==0){

                vi.setBackgroundColor(Color.RED);
            }



            holder.authorphoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Toast.makeText(activity, data.get(position).getDobTime(), Toast.LENGTH_SHORT).show();
//                    String value = (arrayDabba).get(i).toString();

                    String s=data.get(position).getAuthorName();


                    activity.startActivity(new Intent(activity, MainActivity.class)
                            .putExtra("list",s));


                }
            });

//            holder.dobText.setText(data.get(position).getDobTime());

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.authorName.setText("No Data");

        }

        return vi;
    }



  }




//
// extends ArrayAdapter<Model> {
//
//    ArrayList<Model>  arrayList = new ArrayList<Model>();
//    Context context;
//
//    private final LayoutInflater inflater;
//
//    public MyAdapter(@NonNull Context context, ArrayList<Model> arrayList) {
//        super(context, 0, arrayList);
//        this.context=context;
//        inflater = (LayoutInflater) context.
//                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.arrayList = arrayList;
//
//    }
//
//
//    @NonNull
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        View view= convertView;
//
//        if(convertView==null) {
//
//            view = inflater.inflate(R.layout.jadu, null);
//            Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
//            ImageView authorphoto = view.findViewById(R.id.authorImage);
//            TextView authorName=view.findViewById(R.id.authorName);
//            TextView dobText=view.findViewById(R.id.dobText);
//            TextView  deathText=view.findViewById(R.id.deathText);
//            TextView biography= view.findViewById(R.id.biographyText);
//            TextView books= view.findViewById(R.id.bookstext);
//
//
//            authorphoto.setImageResource(arrayList.get(position).getImage());
//            authorName.setText(arrayList.get(position).getAuthorName());
//            dobText.setText(arrayList.get(position).getDobTime());
//            deathText.setText(arrayList.get(position).getDeathTime());
//            biography.setText(arrayList.get(position).getAuthorBiography());
//            books.setText(arrayList.get(position).getAuthorBooks());
//
//
//        }
//
//
//        return view;
//
//
//    }
//}
