package com.arun.aashu.authorbio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AuthorActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Model> arrayDabba;
    int i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);

        listView = findViewById(R.id.mylist);

        arrayDabba = new ArrayList<Model>();

        Model chetan = new Model("Chetan Bhagat",
                "Born: 22 April 1974",
                "Alive Now",
                R.drawable.chetan,

                "FAMOUS FOR : Chetan Bhagat (born 22 April 1974) is an Indian author,\n" +
                        "        columnist, screenwriter, television personality\n" +
                        "        and motivational speaker, known for his English-language\n" +
                        "        dramedy novels about young urban middle-class Indians",

                "Books Written :\n" +
                        "Half Girlfriend\n" +
                        "One Indian Girl\n" +
                        "Five Point Someone\n" +
                        "2 States: The Story of My Marriage\n" +
                        "The 3 Mistakes of My Life\n" +
                        "One Night at the Call Center: A Novel\n" +
                        "Revolution 2020\n" +
                        "What Young India Wants\n" +
                        "Making India Awesome: New Essays and Columns");


        Model jaishankar = new Model("Jaishankar Prasad",
                "Born: 30 January 1890",
                "Died: 15 November 1937",
                R.drawable.jaishankar,
                "FAMOUS FOR :" +
                        "He (30 January 1890  – 15 November 1937) was a famed figure in modern Hindi literature" +
                        " as well as Hindi theatre." +
                        "His dramas are considered to be most pioneering ones in Hindi \n " +
                        "Prasad's most famous dramas include Skandagupta, Chandragupta and Dhruvaswamini",

                "Poetry Written :\n" +
                        "Kānan kusum (The Forest Flower)\n" +
                        "Mahārānā kā mahatv (The Maharana's greatness)\n" +
                        "Jharnā (The Waterfall)\n" +
                        "Ānsū (The tear)\n" +
                        "Lahar (The wave)\n" +
                        "Kāmāyanī (an epic about Manu and the flood)\n" +
                        "Prem pathik (The Love Wanderer)\n" +
                        "Aatmkathya (Autobiography)"
        );


        Model kabir = new Model("Kabir Das",
                " Born: 1440",
                " Died:  1518",
                R.drawable.kabir,

                "FAMOUS FOR : Kabir (Hindi: कबीर,) was a 15th-century Indian mystic poet and saint," +
                        " whose writings influenced Hinduism's Bhakti movement and his verses are found in " +
                        "Sikhism's scripture Guru Granth Sahib.\n" +
                        " His early life was in a Muslim family,but he was strongly influenced by his teacher," +
                        " the Hindu bhakti leader Ramananda.\n" +
                        "Kabir is known for being critical of both Hinduism and Islam, stating that the " +
                        "former was misguided by the Vedas, " +
                        "and questioning their meaningless rites of initiation such as the sacred thread " +
                        "and circumcision respectively",

                "Books Written :\n" +
                        "The Kabir book\n" +
                        "Bijak" );


        Model kumar_Vishwas = new Model("Kumar Vishwas",
                "Born: 10 February 1970 (age 48)",
                "Alive Now",
                R.drawable.kumarvishwas,

                "FAMOUS FOR : Kumar Vishwas (born 10 February 1970) is a Hindi-language performance poet " +
                        "and an Indian politician and National Executive of Aam Aadmi Party",

                "Poem Written :\n" +
                        " 1) Koi Deewana Kahta hai..\n" +
                        " 2) Hum hai Deshi..\n" +
                        " 3) Hontho pe Ganga ho..\n" +
                        " 4) Pagli Ladki..\n" +
                        " 5) Har bar uski ka chehra." );


        Model prem = new Model("Munshi Premchand",
                "Born: 31 July 1880",
                "Died: 8 October 1936",
                R.drawable.prem,

                "FAMOUS FOR :" +
                        "Munshi Premchand was an Indian writer famous for his modern Hindi-Urdu literature." +
                        "He is one of the most celebrated writers of the Indian subcontinent " +
                        "and is regarded as one of the foremost Hindustani writers of the early twentieth century." +
                        " He began writing under the pen name Nawab Rai," +
                        "but subsequently switched to Premchand, Munshi being an honorary prefix.",

                "Books Written :\n" +
                        " Devasthan Rahasya\n" +
                        " Prema\n" +
                        " Kishna\n" +
                        " Roothi Rani\n" +
                        " Soz-e-Watan\n" +
                        " Vardaan\n" +
                        " Seva Sadan\n" +
                        " Premashram\n" +
                        " Rangbhoomi\n" +
                        " Nirmala\n" +
                        " Kaayakalp\n" +
                        " Pratigya\n" +
                        " Gaban\n" +
                        " Karmabhoomi\n" +
                        " Godaan\n" +
                        " Mangalsootra" );


        Model ramdhari = new Model("Ramdhari Singh Dinkar",
                "Born:  23 September 1908",
                "Died:  24 April 1974",
                R.drawable.ramdhari,

                " FAMOUS FOR :" +
                        "Ramdhari Singh 'Dinkar' (23 September 1908 – 24 April 1974) was an Indian Hindi poet," +
                        " essayist, patriot and academic, who is considered as one of the most important modern Hindi poets." +
                        " He remerged as a poet of rebellion as a consequence of his nationalist poetry written in the days" +
                        " before Indian independence.",

                "Books Written :\n" +
                        " 1).Rashmirathi\n" +
                        " 2).Urvashi\n" +
                        " 3).Parshuram Ki Pratiksha\n" +
                        " 4).Sanskriti Ke Char Adhyaya" );


        Model tulsidas = new Model("Tulsidas",
                "Born: 1511",
                "Died:  1623",
                R.drawable.tulsidas,
                "FAMOUS FOR :" +
                        "Tulsidas wrote several popular works in Sanskrit and Awadhi;" +
                        "he is best known as the author of the epic Ramcharitmanas, a retelling of " +
                        "the Sanskrit Ramayana based on Rama's life in the vernacular Awadhi dialect of Hindi. ..." +
                        " Tulsidas spent most of his life in the city of Varanasi.",

                " Books Written :\n" +
                        " Ramcharitmanas\n" +
                        " Shri Ramacharitamanasa\n" +
                        " Tulsi Dohawali\n" +
                        " The Rámáyana of Tulsi Dás" );


//        arrayDabba.add(chetan +"Second Author\n"+ jaishankar +"Third Author"+kabir +
//                "Fourth Author"+kumar_Vishwas+
//                "Fith Author"+prem+
//                "Sixth Author"+ramdhari+
//        "Seventh Author"+tulsidas+);

        arrayDabba.add(chetan);
        arrayDabba.add(jaishankar);
        arrayDabba.add(kabir);
        arrayDabba.add(kumar_Vishwas);
        arrayDabba.add(prem);
        arrayDabba.add(ramdhari);
        arrayDabba.add(tulsidas);

        Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();

        MyAdapter adapter = new MyAdapter(AuthorActivity.this, arrayDabba);

        listView.setAdapter(adapter);

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                String value = (arrayDabba).get(i).toString();
//
//
//                startActivity(new Intent(AuthorActivity.this, MainActivity.class)
//                        .putExtra("listValue",value)
//                );
//
//
//            }
//        });

    }
}
