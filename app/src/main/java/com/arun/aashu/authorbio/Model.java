package com.arun.aashu.authorbio;

/**
 * Created by aAsHu on 3/24/2018.
 */

public class Model  {

    String authorName;
    String DobTime;
    String DeathTime;
    Integer image;
    String  authorBiography;
    String  authorBooks;

    public Model() {
    }


    public Model(String authorName, String dobTime, String deathTime, Integer image, String authorBiography, String authorBooks) {
        this.authorName = authorName;
        this.DobTime = dobTime;
        this.DeathTime = deathTime;
        this.image = image;
        this.authorBiography = authorBiography;
        this.authorBooks = authorBooks;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getDobTime() {
        return DobTime;
    }

    public void setDobTime(String dobTime) {
        this.DobTime = dobTime;
    }

    public String getDeathTime() {
        return DeathTime;
    }

    public void setDeathTime(String deathTime) {
        this.DeathTime = deathTime;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getAuthorBiography() {
        return authorBiography;
    }

    public void setAuthorBiography(String authorBiography) {
        this.authorBiography = authorBiography;
    }

    public String getAuthorBooks() {
        return authorBooks;
    }

    public void setAuthorBooks(String authorBooks) {
        this.authorBooks = authorBooks;
    }
}
